/** @format */

const selected = document.querySelector(".selected");
const optionsContainer = document.querySelector(".options-container");

const optionsList = document.querySelectorAll(".option");

let active = false;

selected.addEventListener("click", () => {
  if (active == false) {
    active = true;
    optionsContainer.classList.add("active");
    console.log(active);
  } else if (active == true) {
    optionsContainer.classList.remove("active");
    active = false;
  }
});
// selected.addEventListener("movee", () => {
//   optionsContainer.classList.toggle("active");
// });

window.addEventListener("mouseup", (event) => {
  if (event.target != optionsContainer) {
    optionsContainer.classList.remove("active");
    // active = false;
  }
});
optionsList.forEach((o) => {
  o.addEventListener("click", () => {
    selected.innerHTML = o.querySelector("label").innerHTML;
    optionsContainer.classList.remove("active");
    active = false;
  });
});

const toggleButton2 = document.getElementsByTagName("checkbox");

toggleButton2.addEventListener("click", () => {
  console.log("checked");
});
